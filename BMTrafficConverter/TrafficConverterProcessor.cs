﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using BMTrafficConverter.Input;

namespace BMTrafficConverter
{
    public class TrafficConverterProcessor
    {
        private static List<FeedAgent> _feedAgents = new List<FeedAgent>();

        #region Logging Definition
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        #endregion

        public static void Start()
        {
            Log.InfoFormat("Starting the BMTrafficConverter service...");
            if (!ReadConfig(Properties.Settings.Default.ConfigPath))
            {
                throw new Exception("Error in configuration!");
            }
            foreach (var feedAgent in _feedAgents)
            {
                feedAgent.Start();
            }
        }

        public static void Stop()
        {
            Log.InfoFormat("Stopping the BMTrafficConverter service...");
            foreach (var feedAgent in _feedAgents)
            {
                feedAgent.Stop();
            }
        }

        private static bool ReadConfig(string configPath)
        {
            var configDocument = XDocument.Load(configPath);
            _feedAgents = configDocument.Root.Elements("Feed").Select(v => new FeedAgent
            {
                Directory = v.Attribute("directory").Value,
                DestinationHost = v.Attribute("destinationAddress").Value,
                InputId = int.Parse(v.Attribute("inputId").Value),
                Filter = v.Attribute("filter").Value,
                Parser = ParserFactory.GetParserType(v.Attribute("parser").Value),
                Archive = v.Attribute("archive") != null && v.Attribute("archive").Value == "1"
            }).ToList();
            return true;
        }
    }
}

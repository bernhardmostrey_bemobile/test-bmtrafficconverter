﻿using System;
using System.IO;
using System.Threading;
using BeMobile.Protobuf;
using BeMobile.Protobuf.TrafficEvents;
using BeMobile.ZMQ;
using BMTrafficConverter.Input;

namespace BMTrafficConverter
{
    public class FeedAgent
    {
        private readonly FileSystemWatcher _fileSystemWatcher = new FileSystemWatcher();
        private Pusher _pusher;
        private IParser _parser;

        public string DestinationHost { get; set; }
        public int InputId { get; set; }
        public ParserType Parser { get; set; }
        public string Directory { get; set; }
        public string Filter { get; set; }
        public bool Archive { get; set; }

        private string _archivePath;

        private const int NumberOfRetries = 5;
        private const int DelayRetry = 10000;

        #region Logging Definition
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        #endregion

        public void Start()
        {
            _pusher = new Pusher(DestinationHost);
            _pusher.Start();
            _parser = ParserFactory.GetParser(Parser);
            _fileSystemWatcher.NotifyFilter = NotifyFilters.FileName;
            _fileSystemWatcher.Path = Directory;
            _fileSystemWatcher.IncludeSubdirectories = false;
            _fileSystemWatcher.Filter = Filter;
            _fileSystemWatcher.Created += FileSystemWatcherCreated;
            _fileSystemWatcher.Renamed += FileSystemWatcherRenamed;
            _fileSystemWatcher.Error += FileSystemWatcherError;
            _fileSystemWatcher.EnableRaisingEvents = true;
            RemoveAllFiles(Directory);
            _archivePath = Path.Combine(Directory, "archive");
            if (!System.IO.Directory.Exists(_archivePath))
            {
                System.IO.Directory.CreateDirectory(_archivePath);
            }
            Log.InfoFormat("Started feed agent for {0}, scanning {1}", InputId, Directory);
        }

        private void RemoveAllFiles(string directory)
        {
            var directoryInfo = new DirectoryInfo(directory);
            foreach (var file in directoryInfo.EnumerateFiles(Filter))
            {
                try
                {
                    file.Delete();
                }
                catch (Exception ex)
                {
                    Log.WarnFormat("Error deleting file " + file.FullName, ex);
                }
            }
        }

        public void Stop()
        {
            if (_pusher != null)
                _pusher.Stop();
            _parser = null;
            Log.InfoFormat("Stopped feed agent (#{0})", InputId);
        }

        void FileSystemWatcherError(object sender, ErrorEventArgs e)
        {
            Log.ErrorFormat("Error occured in filesystemwatcher (#{0}/{1}): {2}", InputId, Directory, e.GetException());
        }

        void FileSystemWatcherRenamed(object sender, RenamedEventArgs e)
        {
            ProcessFile(e.FullPath);
        }

        void FileSystemWatcherCreated(object sender, FileSystemEventArgs e)
        {
            if (e.ChangeType != WatcherChangeTypes.Created) return;
            var fi = new FileInfo(e.FullPath);
            for (var i = 0; i < NumberOfRetries; i++)
            {
                if (IsFileLocked(fi))
                {
                    Thread.Sleep(DelayRetry);
                }
                else
                {
                    ProcessFile(e.FullPath);
                    break;
                }
            }
        }

        private void ProcessFile(string location)
        {
            Log.DebugFormat("File detected for input {0}: {1}", InputId, location);
            try
            {
                var listOfObjects = _parser.ParseDocument(location);
                var trafficContainer = new TrafficContainer
                                           {
                                               InputId = InputId,
                                               TrafficEvents = listOfObjects
                                           };
                _pusher.SendMessage(SerializeHelper.Serialize(trafficContainer));
                if (Archive)
                {
                    var file = new FileInfo(location);
                    var archiveFileName = file.Name.Replace(file.Extension, string.Format(".{0}{1}", DateTime.UtcNow.ToString("yyyyMMddHHmmss"), file.Extension));
                    File.Move(location, Path.Combine(_archivePath, archiveFileName));
                }
                else
                {
                    File.Delete(location);
                }
            }
            catch (Exception ex)
            {
                Log.Error("Error processing file: " + location, ex);
            }
        }

        // I've got this from the internet, but lost the source link.
        // This should be the fastest way to detect if a file is ready to be read.
        static bool IsFileLocked(FileInfo file)
        {
            FileStream stream = null;
            try
            {
                stream = file.Open(FileMode.Open, FileAccess.Read, FileShare.None);
            }
            catch (IOException)
            {
                //the file is unavailable because it is:
                //still being written to
                //or being processed by another thread
                //or does not exist (has already been processed)
                return true;
            }
            finally
            {
                if (stream != null)
                    stream.Close();
            }

            //file is not locked
            return false;
        }
    }
}

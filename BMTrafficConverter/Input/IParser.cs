﻿using System.Collections.Generic;
using BeMobile.Protobuf.TrafficEvents;

namespace BMTrafficConverter.Input
{
    public interface IParser
    {
        List<TrafficEvent> ParseDocument(string location);
    }
}

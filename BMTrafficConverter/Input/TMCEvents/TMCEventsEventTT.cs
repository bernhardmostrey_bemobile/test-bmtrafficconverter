using System;

namespace BMTrafficConverter.Input.TMCEvents
{
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [SerializableAttribute]
    [System.Diagnostics.DebuggerStepThroughAttribute]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public class TMCEventsEventTT
    {
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("hhmmss", DataType = "time")]
        public DateTime Hhmmss { get; set; }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("totalSeconds")]
        public ushort TotalSeconds { get; set; }
    }
}
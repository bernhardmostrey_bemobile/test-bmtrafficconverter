namespace BMTrafficConverter.Input.TMCEvents
{
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute]
    [System.Diagnostics.DebuggerStepThroughAttribute]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public class TMCEventsEventTMCLocation
    {
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute]
        public uint Primary { get; set; }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute]
        public uint Secondary { get; set; }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute]
        public bool SecondarySpecified { get; set; }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute]
        public byte Extent { get; set; }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute]
        public bool ExtentSpecified { get; set; }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute]
        public byte TMCCodingDirection { get; set; }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute]
        public ushort DistanceFromPrimary { get; set; }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute]
        public bool DistanceFromPrimarySpecified { get; set; }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute]
        public uint Length { get; set; }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute]
        public bool LengthSpecified { get; set; }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute]
        public bool InBothDirections { get; set; }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute]
        public bool InBothDirectionsSpecified { get; set; }
    }
}
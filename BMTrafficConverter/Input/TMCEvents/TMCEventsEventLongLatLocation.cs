namespace BMTrafficConverter.Input.TMCEvents
{
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public class TMCEventsEventLongLatLocation
    {
        /// <remarks/>
        public TMCEventsEventLongLatLocationPoint Primary { get; set; }

        /// <remarks/>
        public TMCEventsEventLongLatLocationPoint Secondary { get; set; }
    }
}
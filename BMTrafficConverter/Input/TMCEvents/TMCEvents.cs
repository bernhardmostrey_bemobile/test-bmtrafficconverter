namespace BMTrafficConverter.Input.TMCEvents
{
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute]
    [System.Diagnostics.DebuggerStepThroughAttribute]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public class TMCEvents
    {
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("Event")]
        public TMCEventsEvent[] Event { get; set; }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute]
        public ulong FileTimeStamp { get; set; }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute]
        public string Source { get; set; }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute]
        public string Country { get; set; }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute]
        public byte LocationTableNumber { get; set; }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute]
        public string LocationTableVersion { get; set; }
    }
}
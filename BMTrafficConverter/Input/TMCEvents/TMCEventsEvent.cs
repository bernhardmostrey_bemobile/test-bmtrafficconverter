namespace BMTrafficConverter.Input.TMCEvents
{
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute]
    [System.Diagnostics.DebuggerStepThroughAttribute]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public class TMCEventsEvent
    {
        /// <remarks/>
        public TMCEventsEventTT OptimalTT { get; set; }

        /// <remarks/>
        public TMCEventsEventTT ActualTT { get; set; }

        /// <remarks/>
        public TMCEventsEventTT Delay { get; set; }

        /// <remarks/>
        public TMCEventsEventSpeed Speed { get; set; }

        /// <remarks/>
        public TMCEventsEventEvolution Evolution { get; set; }

        /// <remarks/>
        public TMCEventsEventTMCLocation TMCLocation { get; set; }

        /// <remarks/>
        public TMCEventsEventLongLatLocation LongLatLocation { get; set; }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("Text")]
        public TMCEventsEventText[] Text { get; set; }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("EventCode")]
        public TMCEventsEventEventCode[] EventCode { get; set; }

        [System.Xml.Serialization.XmlElementAttribute("BaseMap")]
        public XmlBasemapEntry[] BaseMap { get; set; }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute]
        public long ID { get; set; }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute]
        public string GUID { get; set; }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute]
        public string VersionGUID { get; set; }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute]
        public uint VersionNumber { get; set; }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute]
        public bool VersionNumberSpecified { get; set; }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute]
        public ulong CreatedTS { get; set; }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute]
        public ulong ModifiedTS { get; set; }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute]
        public ulong StartTS { get; set; }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute]
        public ulong EndTS { get; set; }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute]
        public byte IconCode { get; set; }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute]
        public bool IconCodeSpecified { get; set; }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute]
        public string DailyActivateInverval { get; set; }
    }

    public class XmlBasemapEntry
    {
        [System.Xml.Serialization.XmlAttributeAttribute("mapid")]
        public string MapId { get; set; }

        [System.Xml.Serialization.XmlText]
        public string Segments { get; set; }
    }
}
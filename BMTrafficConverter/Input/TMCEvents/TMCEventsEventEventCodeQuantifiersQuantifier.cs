﻿
namespace BMTrafficConverter.Input.TMCEvents
{
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute]
    [System.Diagnostics.DebuggerStepThroughAttribute]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public class TMCEventsEventEventCodeQuantifiersQuantifier
    {
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute]
        public string Name { get; set; }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute]
        public decimal Value { get; set; }
    }

}

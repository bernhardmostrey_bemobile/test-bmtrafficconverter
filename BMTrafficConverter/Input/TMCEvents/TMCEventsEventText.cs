namespace BMTrafficConverter.Input.TMCEvents
{
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute]
    [System.Diagnostics.DebuggerStepThroughAttribute]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public class TMCEventsEventText
    {
        /// <remarks/>
        public string RoadText { get; set; }

        /// <remarks/>
        public string DirectionText { get; set; }

        /// <remarks/>
        public string SegmentText { get; set; }

        /// <remarks/>
        public string EventText { get; set; }

        /// <remarks/>
        public string FreeText { get; set; }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute]
        public string Lan { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Xml.Serialization;
using BeMobile.Global;
using BeMobile.Protobuf.TrafficEvents;

namespace BMTrafficConverter.Input
{
    public class TrafficEventsXMLParser : IParser
    {
        #region Logging Definition

        private static readonly log4net.ILog Log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        #endregion

        public List<TrafficEvent> ParseDocument(string location)
        {
            var list = new List<TrafficEvent>();
            try
            {
                var doc = XDocument.Load(location);
                doc.Root.Elements("Event").Where(v => v.Attribute("IconCode") != null && string.IsNullOrEmpty(v.Attribute("IconCode").Value)).Remove();
                // Remove incorrect messages
                var formatter = new XmlSerializer(typeof(TMCEvents.TMCEvents));
                var tmcEvents = formatter.Deserialize(doc.CreateReader()) as TMCEvents.TMCEvents;
                var country = tmcEvents.Country;
                var src = tmcEvents.Source;
                if (tmcEvents.Event == null || tmcEvents.Event.Length == 0)
                {
                    Log.WarnFormat("No events found in {0}", location);
                    return list;
                }
                var processed = new List<long>();
                foreach (var tmcEventsEvent in tmcEvents.Event)
                {
                    var basemaps = tmcEventsEvent.BaseMap == null || tmcEventsEvent.BaseMap.Length == 0
                        ? null
                        : tmcEventsEvent.BaseMap.Select(xmlBasemapEntry => new BasemapEntry
                    {
                        MapId = int.Parse(xmlBasemapEntry.MapId),
                        Segments = xmlBasemapEntry.Segments.Split(new[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(int.Parse).ToList()
                    }).ToList();
                    if (processed.Contains(tmcEventsEvent.ID))
                    {
                        Console.WriteLine("DUPLICATE KEY:" + tmcEventsEvent.ID);
                    }
                    else
                    {
                        processed.Add(tmcEventsEvent.ID);
                    }
                    var trafficEvent = new TrafficEvent
                                           {
                                               MessageId = tmcEventsEvent.ID,
                                               //VersionId = tmcEventsEvent.VersionNumberSpecified ? (int)tmcEventsEvent.VersionNumber : -1,
                                               TMCPrimary = (int)tmcEventsEvent.TMCLocation.Primary,
                                               TMCSecondary = tmcEventsEvent.TMCLocation.SecondarySpecified
                                                                  ? (int)tmcEventsEvent.TMCLocation.Secondary
                                                                  : -1,
                                               TMCCodingDirection = tmcEventsEvent.TMCLocation.TMCCodingDirection == 1,
                                               AlertCs = tmcEventsEvent.EventCode.Select(v => (int)v.AlertC).ToList(),
                                               CreatedDateTime = tmcEventsEvent.CreatedTS != 0
                                                                     ? TimeTools.ConvertDateToUnixEpoch(
                                                                         FromBeMobileDate(tmcEventsEvent.CreatedTS))
                                                                     : -1,
                                               ModifiedDateTime = tmcEventsEvent.ModifiedTS != 0
                                                                      ? TimeTools.ConvertDateToUnixEpoch(
                                                                          FromBeMobileDate(tmcEventsEvent.ModifiedTS))
                                                                      : -1,
                                               Country = country,
                                               OptimalTT =
                                                   tmcEventsEvent.OptimalTT != null &&
                                                   tmcEventsEvent.OptimalTT.TotalSeconds != 0
                                                       ? tmcEventsEvent.OptimalTT.TotalSeconds
                                                       : -1,
                                               Delay =
                                                   tmcEventsEvent.Delay != null &&
                                                   tmcEventsEvent.Delay.TotalSeconds != 0
                                                       ? tmcEventsEvent.Delay.TotalSeconds
                                                       : -1,
                                               ActualTT =
                                                   tmcEventsEvent.ActualTT != null &&
                                                   tmcEventsEvent.ActualTT.TotalSeconds != 0
                                                       ? tmcEventsEvent.ActualTT.TotalSeconds
                                                       : -1,
                                               Evolution =
                                                   tmcEventsEvent.Evolution != null
                                                       ? tmcEventsEvent.Evolution.Sign
                                                       : null,
                                               Speed =
                                                   tmcEventsEvent.Speed != null ? tmcEventsEvent.Speed.Kph : (short)-1,
                                               DistanceFromPrimary =
                                                   tmcEventsEvent.TMCLocation.DistanceFromPrimarySpecified
                                                       ? tmcEventsEvent.TMCLocation.DistanceFromPrimary
                                                       : -1,
                                               Length = tmcEventsEvent.TMCLocation.LengthSpecified
                                                            ? (int)tmcEventsEvent.TMCLocation.Length
                                                            : -1,
                                               Source = src,
                                               StartDateTime = tmcEventsEvent.StartTS != 0
                                                                   ? TimeTools.ConvertDateToUnixEpoch(
                                                                       FromBeMobileDate(tmcEventsEvent.StartTS))
                                                                   : -1,
                                               EndDateTime = tmcEventsEvent.EndTS != 0
                                                                 ? TimeTools.ConvertDateToUnixEpoch(
                                                                     FromBeMobileDate(tmcEventsEvent.EndTS))
                                                                 : -1,
                                               InBothDirections = tmcEventsEvent.TMCLocation.InBothDirectionsSpecified && tmcEventsEvent.TMCLocation.InBothDirections,
                                               Segments = basemaps
                                           };
                    list.Add(trafficEvent);
                }
                return list;
            }
            catch (Exception ex)
            {
                Log.Error("General error serializing TrafficEventsXML", ex);
                return null;
            }
        }

        public static DateTime FromBeMobileDate(ulong dateTime)
        {
            var str = String.Format("{0:00000000000000}", dateTime);
            return DateTime.ParseExact(str, "ddMMyyyyHHmmss", null);
        }
    }
}

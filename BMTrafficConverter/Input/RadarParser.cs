﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Xml.Serialization;
using BeMobile.Global;
using BeMobile.Protobuf;
using BeMobile.Protobuf.TrafficEvents;

namespace BMTrafficConverter.Input
{
    public class RadarParser : IParser
    {
        #region Logging Definition
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        #endregion

        public List<TrafficEvent> ParseDocument(string location)
        {
            var list = new List<TrafficEvent>();
            try
            {
                var doc = XDocument.Load(location);
                var formatter = new XmlSerializer(typeof(Speedtraps.Speedtraps));
                var tmcEvents = formatter.Deserialize(doc.CreateReader()) as Speedtraps.Speedtraps;
                if (tmcEvents.Speedtrap == null || tmcEvents.Speedtrap.Length == 0)
                {
                    Log.WarnFormat("No events found in {0}", location);
                    return list;
                }
                foreach (var speedtrapsspeedtrap in tmcEvents.Speedtrap)
                {
                    try
                    {
                        var createdTS = DateTime.ParseExact(speedtrapsspeedtrap.CreatedTS, "ddMMyyyyHHmmss", null).ToUniversalTime();
                        var modifiedTS = DateTime.ParseExact(speedtrapsspeedtrap.ModifiedTS, "ddMMyyyyHHmmss", null).ToUniversalTime();

                        var endTS = !string.IsNullOrEmpty(speedtrapsspeedtrap.EndTS)
                                        ? TimeTools.ConvertDateToUnixEpoch(DateTime.ParseExact(
                                            speedtrapsspeedtrap.EndTS,
                                            "ddMMyyyyHHmmss", null).ToUniversalTime())
                                        : -1;
                        var trafficEvent = new TrafficEvent
                                               {
                                                   MessageId = speedtrapsspeedtrap.ID,
                                                   CardinalDirection = speedtrapsspeedtrap.CardinalDirection,
                                                   Source = speedtrapsspeedtrap.Source,
                                                   PrimaryLocation = new GeoPoint
                                                                         {
                                                                             Longitude = (float)speedtrapsspeedtrap.Long,
                                                                             Latitude = (float)speedtrapsspeedtrap.Lat
                                                                         },
                                                   SecondaryLocations =
                                                       speedtrapsspeedtrap.SecondaryCoordinates != null
                                                           ? speedtrapsspeedtrap.SecondaryCoordinates.Select(
                                                               v => new GeoPoint
                                                                        {
                                                                            Longitude = (float)v.Long,
                                                                            Latitude = (float)v.Lat
                                                                        }).ToList()
                                                           : null,
                                                   CreatedDateTime = TimeTools.ConvertDateToUnixEpoch(createdTS),
                                                   ModifiedDateTime = TimeTools.ConvertDateToUnixEpoch(modifiedTS),
                                                   EndDateTime = endTS,
                                                   FreeText =
                                                       speedtrapsspeedtrap.Text.Select(
                                                           v => new KeyValue { Key = v.Lan, Value = v.Freetext }).ToList(),
                                                   ContentText =
                                                       speedtrapsspeedtrap.Text.Where(
                                                           v => !string.IsNullOrEmpty(v.Content))
                                                       .Select(v => new KeyValue { Key = v.Lan, Value = v.Content }).
                                                       ToList(),
                                                   SegmentText =
                                                       speedtrapsspeedtrap.Text.Where(
                                                           v => !string.IsNullOrEmpty(v.Segment))
                                                       .Select(v => new KeyValue { Key = v.Lan, Value = v.Segment }).
                                                       ToList(),
                                                   AlertCs = new List<int> { 1853 }
                                               };
                        list.Add(trafficEvent);
                    }
                    catch (Exception ex)
                    {
                        Log.Error("Error occured at converting event", ex);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error("General error serializing document!", ex);
            }
            return list;
        }
    }
}

﻿
namespace BMTrafficConverter.Input.Speedtraps
{
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute]
    [System.Diagnostics.DebuggerStepThroughAttribute]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public class SpeedtrapText
    {
        /// <remarks/>
        [System.Xml.Serialization.XmlElement("SEGMENT")]
        public string Segment { get; set; }

        /// <remarks/>
        [System.Xml.Serialization.XmlElement("CONTENT")]
        public string Content { get; set; }

        /// <remarks/>
        [System.Xml.Serialization.XmlElement("FREETEXT")]
        public string Freetext { get; set; }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute]
        public string Lan { get; set; }
    }
}

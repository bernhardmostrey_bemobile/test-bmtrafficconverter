using System.Xml.Serialization;

namespace BMTrafficConverter.Input.Speedtraps
{
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute]
    [System.Diagnostics.DebuggerStepThroughAttribute]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [XmlType(AnonymousType=true)]
    [XmlRoot(ElementName = "SPEEDTRAPS",Namespace="", IsNullable=false)]
    public class Speedtraps {
        /// <remarks/>
        [XmlElement("SPEEDTRAP")]
        public Speedtrap[] Speedtrap { get; set; }

        /// <remarks/>
        [XmlAttribute]
        public string FileTimeStamp { get; set; }
    }
}
namespace BMTrafficConverter.Input.Speedtraps
{
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute]
    [System.Diagnostics.DebuggerStepThroughAttribute]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public class SpeedtrapCoordinate {
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute]
        public decimal Long { get; set; }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute]
        public decimal Lat { get; set; }
    }
}
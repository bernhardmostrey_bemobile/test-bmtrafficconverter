namespace BMTrafficConverter.Input.Speedtraps
{
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute]
    [System.Diagnostics.DebuggerStepThroughAttribute]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public class Speedtrap {
        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("COORDINATE", IsNullable=false)]
        public SpeedtrapCoordinate[] SecondaryCoordinates { get; set; }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("TEXT")]
        public SpeedtrapText[] Text { get; set; }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute]
        public uint ID { get; set; }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute]
        public decimal Long { get; set; }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute]
        public decimal Lat { get; set; }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute]
        public string CardinalDirection { get; set; }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute]
        public string CreatedTS { get; set; }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute]
        public string ModifiedTS { get; set; }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute]
        public string EndTS { get; set; }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute]
        public string Source { get; set; }
    }
}
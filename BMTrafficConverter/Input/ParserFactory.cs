﻿
namespace BMTrafficConverter.Input
{
    public class ParserFactory
    {
        public static IParser GetParser(ParserType paserType)
        {
            switch (paserType)
            {
                case ParserType.SpeedTrapsProvider:
                    return new RadarParser();
                    case ParserType.TrafficEventsXMLProvider:
                    return new TrafficEventsXMLParser();
                default:
                    return null;
            }
        }

        public static ParserType GetParserType(string parser)
        {
            switch (parser.ToLower())
            {
                case "speedtraps":
                case "radars":
                    return ParserType.SpeedTrapsProvider;
                case "trafficeventsxml":
                    return ParserType.TrafficEventsXMLProvider;
                default:
                    return ParserType.TrafficEventsXMLProvider;
            }
        }
    }

    public enum ParserType
    {
        SpeedTrapsProvider,
        TrafficEventsXMLProvider
    }
}

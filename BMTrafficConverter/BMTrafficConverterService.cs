﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;

namespace BMTrafficConverter
{
    public partial class BMTrafficConverterService : ServiceBase
    {
        public BMTrafficConverterService()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            TrafficConverterProcessor.Start();
        }

        protected override void OnStop()
        {
            TrafficConverterProcessor.Stop();
        }
    }
}

﻿using System;
using System.IO;
using System.Threading;
using BeMobile.Protobuf;
using BeMobile.Protobuf.TrafficEvents;
using BeMobile.ZMQ;
using BMTrafficConverter.Input;

namespace BMTrafficConverter.CLI
{
    class Program
    {
        private static Pusher _pusher;
        private static IParser _parser;
        private static ParserType _parserType;

        static void Main(string[] args)
        {
#if DEBUG
            args = new[] { @"C:\tmp\BETMC(Be-Mobile TMC Event XML).xml", "trafficeventsxml", "tcp://127.0.0.1:8036", "2" };
#endif
            if (args.Length != 4)
            {
                ErrorMessage();
                return;
            }
            var file = args[0];
            var typeOfFile = args[1];
            var zmqHost = args[2];
            int inputId;
            if (!int.TryParse(args[3], out inputId))
            {
                ErrorMessage();
                return;
            }
            if (!File.Exists(file))
            {
                Console.WriteLine("File does not exists");
                return;
            }
            _pusher = new Pusher(zmqHost);
            _pusher.Start();
            _parserType = ParserFactory.GetParserType(typeOfFile);
            _parser = ParserFactory.GetParser(_parserType);
            var listOfObjects = _parser.ParseDocument(file);
            if (listOfObjects == null)
            {
                Console.WriteLine("Could not parse XML!");
                return;
            }
            var trafficContainer = new TrafficContainer
            {
                InputId = inputId,
                TrafficEvents = listOfObjects
            };
            _pusher.SendMessage(SerializeHelper.Serialize(trafficContainer));
            _pusher.Stop();
            Console.WriteLine("Send {0} items to {1} with inputid {2}", listOfObjects.Count, zmqHost, inputId);
            Thread.Sleep(1000);
        }

        private static void ErrorMessage()
        {
            Console.WriteLine("Invalid parameters, usage: BMTrafficConverter.CLI.exe <file> <type> <zmqhost> <inputid>");
        }
    }
}

﻿using BMTrafficConverter.Input;
using NUnit.Framework;

namespace BMTrafficConverter.Tests
{
    [TestFixture]
    public class RadarTests
    {
        private IParser _parser;

        [SetUp]
        public void Setup()
        {
            _parser = new RadarParser();
        }

        [Test]
        public void RadarTest()
        {
            var records1 = _parser.ParseDocument("samples/radar-1.xml");
            Assert.AreEqual(7, records1.Count);
            var records2 = _parser.ParseDocument("samples/radar-2.xml");
            Assert.AreEqual(8, records2.Count);
            var records3 = _parser.ParseDocument("samples/radar-3.xml");
            Assert.AreEqual(16, records3.Count);
        }
    }
}

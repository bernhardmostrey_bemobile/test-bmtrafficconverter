﻿﻿using BMTrafficConverter.Input;
using NUnit.Framework;
using System.Linq;

namespace BMTrafficConverter.Tests
{
    [TestFixture]
    public class TrafficEventsXMLTests
    {
        private IParser _parser;

        [SetUp]
        public void Setup()
        {
            _parser = new TrafficEventsXMLParser();
        }

        [Test]
        public void TestTelenavisXmlParse()
        {
            var records1 = _parser.ParseDocument("samples/telenavis-1.xml");
            Assert.AreEqual(12, records1.Count);
            var records2 = _parser.ParseDocument("samples/telenavis-2.xml");
            Assert.AreEqual(8, records2.Count);
            var records3 = _parser.ParseDocument("samples/telenavis-3.xml");
            Assert.AreEqual(3, records3.Count);
        }

        [Test]
        public void TestMetXmlParse()
        {
            var records1 = _parser.ParseDocument("samples/met-1.xml");
            Assert.AreEqual(115, records1.Count);
            var records2 = _parser.ParseDocument("samples/met-2.xml");
            Assert.AreEqual(114, records2.Count);
            var records3 = _parser.ParseDocument("samples/met-3.xml");
            Assert.AreEqual(112, records3.Count);
        }

        [Test]
        public void TestEmptyXml()
        {
            var records = _parser.ParseDocument("samples/empty.xml");
            Assert.AreEqual(0, records.Count);
        }

        [Test]
        public void TestIcarusXml()
        {
            var records = _parser.ParseDocument("samples/icarus.xml");
            Assert.AreEqual(115, records.Count);
            var records2 = _parser.ParseDocument("samples/icarus2.xml");
            Assert.AreEqual(59, records2.Count);
        }

        [Test]
        public void TestAclXml()
        {
            var records = _parser.ParseDocument("samples/acl.xml");
            Assert.AreEqual(94, records.Count); // 94 - 4 without iconcode

            var records1 = _parser.ParseDocument("samples/ACL-alertcorder.xml");
            Assert.AreEqual(1, records1.Count);
            var message = records1[0];
            Assert.AreEqual(2, message.AlertCs.Count);
            Assert.AreEqual(803, message.AlertCs[0]);
            Assert.AreEqual(401, message.AlertCs[1]);
        }

        [Test]
        public void Test_DEVOPS_1944()
        {
            var records = _parser.ParseDocument("samples/TMC_TrafficEvents.xml");
            Assert.IsNotNull(records);
            var recordWithError = records.SingleOrDefault(r => r.MessageId == -3112);
            Assert.IsNotNull(recordWithError);
            Assert.AreEqual(75024, recordWithError.Length);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace BMTrafficConverter.Debug
{
    class Program
    {
        static void Main(string[] args)
        {
            TrafficConverterProcessor.Start();
            Thread.Sleep(int.MaxValue);
        }
    }
}
